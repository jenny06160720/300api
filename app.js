const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')

require('dotenv/config');

app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => res.send("welcome to backend"))

app.use('/branch', require('./routes/branch'));
app.use('/menu', require('./routes/menu'));

//Connect to DB
mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true },
    () => console.log('connected to DB!')
);


//How to we start Listening the server
app.listen(3000);