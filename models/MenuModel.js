const mongoose = require('mongoose');

const menuStructure = mongoose.Schema({
    enName: {
        type: String,
        required: true
    },
    cnName: {
        type: String,
        required: true,
    },
    hot: {
        type: Boolean,
        required: true
    },
    ice: {
        type: Boolean,
        required: true
    }
})

module.exports = mongoose.model('menu', menuStructure);