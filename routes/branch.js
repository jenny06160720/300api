const express = require('express');
const router = express.Router();
const config = require('config');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const BranchModel = require('../models/BranchModel');

router.get("/", async (req, res) => {
    try {
        res.json(await BranchModel.find());
    } catch (err) {
        res.json({ message: err });
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await BranchModel.findById(req.params.id));
    } catch (err) {
        res.json({ message: err });
    }
});

router.post('/', async (req, res) => {
    const {enlocation, chlocation, lat,lng} = req.body;
    try {
        const branchModel = new BranchModel({
            enlocation,
            chlocation,
            lat,
            lng
        });
        const savePetUser = await branchModel.save();
        res.json(savePetUser);
    } catch(err) {
        console.log(err.message);
        res.status(500).send('Cannot Create New branch')
    }
});

router.delete('/:id', async (req, res) => {
    try {
        res.json(await BranchModel.remove({ _id: req.params.id }));
    } catch (err) {
        res.json({ message: "Cannot Delete branch" });
    }
});


module.exports = router;