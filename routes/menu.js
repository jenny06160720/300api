const express = require('express');
const router = express.Router();
const MenuModel = require('../models/MenuModel');

router.get("/", async (req, res) => {
    try {
        res.json(await MenuModel.find());
    } catch (err) {
        res.json({ message: err });
    }
});

router.get('/:id', async (req, res) => {
    try {
        res.json(await MenuModel.findById(req.params.id));
    } catch (err) {
        res.json({ message: err });
    }
});

router.post('/', async (req, res) => {
    const {enName, cnName, hot,ice} = req.body;
    try {
        const menuModel = new MenuModel({
            enName,
            cnName,
            hot,
            ice
        });
        const saves = await menuModel.save();
        res.json(saves);
    } catch(err) {
        console.log(err.message);
        res.status(500).send('Cannot Create New Menu')
    }
});

router.delete('/:id', async (req, res) => {
    try {
        res.json(await MenuModel.remove({ _id: req.params.id }));
    } catch (err) {
        res.json({ message: "Cannot Delete Menu" });
    }
});


module.exports = router;